import ec.util.MersenneTwisterFast;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import sim.field.grid.SparseGrid2D;
import sim.util.Bag;
import sim.util.Int2D;

import java.util.ArrayList;

import Pathfinding.AStar;

public class Insect implements Steppable {
    private static double coefDepl = 0.0;
    private static double coefPercep = 1;
    public int x, y;
    Stoppable stoppable;
    ArrayList<int[]> env;
    ArrayList<Int2D> path;
    private int energy;
    private int charge;
    private int portee;
    private int stackSize;
    private ArrayList<Int2D> posHistory;
    private Int2D target;
    private int maxIter = 100;

    public Insect() {
        //carac stat
        Constants.DISTANCE_DEPLACEMENT = (int) (1 + Constants.CAPACITY * coefDepl);
        Constants.DISTANCE_PERCEPTION = (int) (1 + Constants.CAPACITY * coefPercep);
        Constants.MAX_LOAD = (int) (1 + Constants.CAPACITY * (1 - coefDepl - coefPercep));

        //carac dyn
        MersenneTwisterFast mtf = new MersenneTwisterFast();
        energy = 3 + mtf.nextInt(Constants.MAX_ENERGY - 3);
        charge = mtf.nextInt(Constants.MAX_LOAD);

        //carac custom
        portee = 1;
        stackSize = 1;

        env = new ArrayList<int[]>(Constants.GRID_SIZE * Constants.GRID_SIZE);
        for (int i = 0; i < Constants.GRID_SIZE * Constants.GRID_SIZE; i++) {
            env.add(new int[]{0, 0});
        }
        posHistory = new ArrayList<>();
        posHistory.add(new Int2D(x, y));
    }

    @Override
    public void step(SimState simState) {
        Beings beings = (Beings) simState;
        if (!isAlive()) {
            remove(beings);
            return;
        }
        percept(beings);

        if (!charge(beings) && !eat(beings)) {
            move(beings);
        }
    }

    private boolean isAlive() {
        return energy > 0;
    }

    private void remove(Beings beings) {
        beings.yard.remove(this);
        stoppable.stop();
        beings.setNumInsects(beings.getNumInsects() - 1);
        System.out.println("MORT !!!!");
    }

    private void percept(Beings beings) {
        Bag b;
        for (int i = 0; i < Constants.DISTANCE_PERCEPTION; i++) {
            for (int j = i + 1; j < Constants.DISTANCE_PERCEPTION; j++) {
                b = beings.yard.getObjectsAtLocation(x + i, y + j);
                updateEnv(b, x + i, y + j);
                b = beings.yard.getObjectsAtLocation(x + j, y + i);
                updateEnv(b, x + j, y + y);
                b = beings.yard.getObjectsAtLocation(x - i, y - j);
                updateEnv(b, x - i, y - j);
                b = beings.yard.getObjectsAtLocation(x - j, y - i);
                updateEnv(b, x - j, y - i);
                b = beings.yard.getObjectsAtLocation(x + i, y - j);
                updateEnv(b, x + i, y - j);
                b = beings.yard.getObjectsAtLocation(x + j, y - i);
                updateEnv(b, x + j, y - i);
                b = beings.yard.getObjectsAtLocation(x - i, y + j);
                updateEnv(b, x - i, y + j);
                b = beings.yard.getObjectsAtLocation(x - j, y + i);
                updateEnv(b, x - j, y + i);
            }
        }
    }

    private void updateEnv(Bag b, int x, int y) {
        if (x < 0 || y < 0 || x >= Constants.GRID_SIZE || y >= Constants.GRID_SIZE) {
            return;
        }
        int[] content = {0, 0};
        if (b != null) {
            for (Object obj : b) {
                if (obj instanceof Insect) {
                    content[0] = 1;
                }
                if (obj instanceof Feed) {
                    content[1] = ((Feed) obj).quantity;
                }
            }
        }
        env.set(x * Constants.GRID_SIZE + y, content);
    }

    private boolean eat(Beings beings) {
        if (energy <= Constants.MAX_ENERGY - Constants.FOOD_ENERGY) {
            if (charge == 0) {
                Int2D foodLoc = checkFeed(portee);
                if (foodLoc != null) {
                    Bag b = beings.yard.getObjectsAtLocation(foodLoc);
                    if (b != null) {
                        for (Object obj : b) {
                            if (obj instanceof Feed) {
                                int min = Math.min(stackSize, ((Feed) obj).quantity);
                                ((Feed) obj).quantity -= min;
                                int min2 = Math.min(min * Constants.FOOD_ENERGY, Constants.MAX_ENERGY - energy);
                                energy += min2;
                                return true;
                            }
                        }
                    }
                }
            } else {
                int min = Math.max(stackSize, charge);
                charge -= min;
                int min2 = Math.min(min * Constants.FOOD_ENERGY, Constants.MAX_ENERGY - energy);
                energy += min2;
                return true;
            }
        }
        return false;
    }

    private Int2D checkFeed(int max_portee) {
        for (int i = 0; i < max_portee; i++) {
            for (int j = 1 + i; j < max_portee; j++) {
                if (checkBorder((x + i), (y + j)) && env.get(Constants.GRID_SIZE * (x + i) + (y + j))[1] > 0) {
                    return new Int2D(x + i, y + j);
                } else if (checkBorder((x + j), (y + i)) && env.get(Constants.GRID_SIZE * (x + j) + (y + i))[1] > 0) {
                    return new Int2D(x + j, y + i);
                } else if (checkBorder((x - i), (y - j)) && env.get(Constants.GRID_SIZE * (x - i) + (y - j))[1] > 0) {
                    return new Int2D(x - i, y - j);
                } else if (checkBorder((x - j), (y - i)) && env.get(Constants.GRID_SIZE * (x - j) + (y - i))[1] > 0) {
                    return new Int2D(x - j, y - i);
                } else if (checkBorder((x + i), (y - j)) && env.get(Constants.GRID_SIZE * (x + i) + (y - j))[1] > 0) {
                    return new Int2D(x + i, y - j);
                } else if (checkBorder((x + j), (y - i)) && env.get(Constants.GRID_SIZE * (x + j) + (y - i))[1] > 0) {
                    return new Int2D(x + j, y - i);
                } else if (checkBorder((x - i), (y + j)) && env.get(Constants.GRID_SIZE * (x - i) + (y + j))[1] > 0) {
                    return new Int2D(x - i, y + j);
                } else if (checkBorder((x - j), (y + i)) && env.get(Constants.GRID_SIZE * (x - j) + (y + i))[1] > 0) {
                    return new Int2D(x - j, y + i);
                }
            }
        }
        return null;
    }


    private void move(Beings beings) {
        Int2D foodLoc = checkFeed(Constants.DISTANCE_PERCEPTION);
        if (foodLoc != null) {
        	if(Constants.DISTANCE_DEPLACEMENT < calcDist(x, y, foodLoc.x, foodLoc.y)) {
        		if (path == null || !target.equals(foodLoc)) {
        			System.out.println("Recherche entre " + this.x + "," + this.y + " et " + foodLoc.toString());
        			path = AStar.pathfinding(beings.yard, new Int2D(x,y), foodLoc);
        			if(path != null) {
        				target = foodLoc;
        				path.remove(path.size()-1);
        			}	
        		}
        		if (path != null && !path.isEmpty()) {
        			Int2D nextLoc = path.get(Math.max(path.size()-1, 0)); // -1 egal pos actuel
            		path.remove(path.size()-1);
            		moveDelta(beings, nextLoc.x - this.x, nextLoc.y - this.y);
        		} else {
        			System.err.print("Help c'est vide");
        		}
        	} else {
        		path=null;
        		if (energy >= 3) { //charger - dessus
                    moveDelta(beings, foodLoc.x - this.x, foodLoc.y - this.y);
                } else { //manger - ﾃ� cﾃｴtﾃｩ
                    Int2D target = getTarget(foodLoc);
                    if (target != null) {
                        moveDelta(beings, target.x - this.x, target.y - this.y);
                    } else {
                        System.out.println("no move");
                    }
                }
        	}
        } else {
            //move alea
            MersenneTwisterFast mtf = new MersenneTwisterFast();
            int deltaX = 0;
            int deltaY = 0;
            maxIter = 100;
            while (deltaX == 0 && deltaY == 0 ||
                    x + deltaX < 0 || y + deltaY < 0 ||
                    x + deltaX >= Constants.GRID_SIZE || y + deltaY >= Constants.GRID_SIZE ||
                    maxIter > 0 && checkDist(x + deltaX, y + deltaY) || containsBlock(beings.yard, new Int2D(x + deltaX, y + deltaY))) {

                deltaX = mtf.nextInt(2 * Constants.DISTANCE_DEPLACEMENT) - Constants.DISTANCE_DEPLACEMENT;
                deltaY = mtf.nextInt(2 * Constants.DISTANCE_DEPLACEMENT) - Constants.DISTANCE_DEPLACEMENT;
                maxIter--;
            }
            moveDelta(beings, deltaX, deltaY);
        }
        energy--;
    }

    private boolean checkDist(int xTo, int yTo) {
        Int2D lastPos = posHistory.get(posHistory.size() - 1);
        if (calcDist(x, y, xTo, yTo) < 0.7 * Constants.DISTANCE_DEPLACEMENT) {
            return false;
        }
        if (calcDist(lastPos.x, lastPos.y, 0, 0) >= Constants.DISTANCE_DEPLACEMENT &&
                calcDist(lastPos.x, lastPos.y, 0, Constants.GRID_SIZE - 1) >= Constants.DISTANCE_DEPLACEMENT &&
                calcDist(lastPos.x, lastPos.y, Constants.GRID_SIZE - 1, 0) >= Constants.DISTANCE_DEPLACEMENT &&
                calcDist(lastPos.x, lastPos.y, Constants.GRID_SIZE - 1, Constants.GRID_SIZE - 1) >= Constants.DISTANCE_DEPLACEMENT) {
            return !(calcDist(lastPos.x, lastPos.y, xTo, yTo) < 0.9 * Constants.DISTANCE_DEPLACEMENT);
        }
        return true;
    }

    private Int2D getTarget(Int2D foodLoc) {
        int d;
        Int2D newLoc;
        newLoc = new Int2D(foodLoc.x - 1, foodLoc.y);
        if (checkBorder(newLoc.x, newLoc.y)) {
            d = calcDist(x, y, newLoc.x, newLoc.y);
            if (d <= Constants.DISTANCE_DEPLACEMENT) {
                return newLoc;
            }
        }
        newLoc = new Int2D(foodLoc.x, foodLoc.y - 1);
        if (checkBorder(newLoc.x, newLoc.y)) {
            d = calcDist(x, y, newLoc.x, newLoc.y);
            if (d <= Constants.DISTANCE_DEPLACEMENT) {
                return newLoc;
            }
        }
        newLoc = new Int2D(foodLoc.x + 1, foodLoc.y);
        if (checkBorder(newLoc.x, newLoc.y)) {
            d = calcDist(x, y, newLoc.x, newLoc.y);
            if (d <= Constants.DISTANCE_DEPLACEMENT) {
                return newLoc;
            }
        }
        newLoc = new Int2D(foodLoc.x, foodLoc.y + 1);
        if (checkBorder(newLoc.x, newLoc.y)) {
            d = calcDist(x, y, newLoc.x, newLoc.y);
            if (d <= Constants.DISTANCE_DEPLACEMENT) {
                return newLoc;
            }
        }
        newLoc = new Int2D(foodLoc.x - 1, foodLoc.y - 1);
        if (checkBorder(newLoc.x, newLoc.y)) {
            d = calcDist(x, y, newLoc.x, newLoc.y);
            if (d <= Constants.DISTANCE_DEPLACEMENT) {
                return newLoc;
            }
        }
        newLoc = new Int2D(foodLoc.x - 1, foodLoc.y + 1);
        if (checkBorder(newLoc.x, newLoc.y)) {
            d = calcDist(x, y, newLoc.x, newLoc.y);
            if (d <= Constants.DISTANCE_DEPLACEMENT) {
                return newLoc;
            }
        }
        newLoc = new Int2D(foodLoc.x + 1, foodLoc.y - 1);
        if (checkBorder(newLoc.x, newLoc.y)) {
            d = calcDist(x, y, newLoc.x, newLoc.y);
            if (d <= Constants.DISTANCE_DEPLACEMENT) {
                return newLoc;
            }
        }
        newLoc = new Int2D(foodLoc.x + 1, foodLoc.y + 1);
        if (checkBorder(newLoc.x, newLoc.y)) {
            d = calcDist(x, y, newLoc.x, newLoc.y);
            if (d <= Constants.DISTANCE_DEPLACEMENT) {
                return newLoc;
            }
        }
        return null;
    }

    private boolean checkBorder(int x, int y) {
        return x > 0 && y > 0 && x < Constants.GRID_SIZE && y < Constants.GRID_SIZE;
    }

    private void moveDelta(Beings beings, int deltaX, int deltaY) {
        beings.yard.remove(this);
        this.x += deltaX;
        this.y += deltaY;
        beings.yard.setObjectLocation(this, x, y);
        posHistory.add(new Int2D(x, y));
    }


    private int calcDist(int xFrom, int yFrom, int xTo, int yTo) {
        return Math.max(Math.abs(yTo - yFrom), Math.abs(xTo - xFrom));
    }

    private boolean charge(Beings beings) {
        Bag b = beings.yard.getObjectsAtLocation(x, y);
        if (b != null) {
            for (Object obj : b) {
                if (obj instanceof Feed) {
                    int min = Math.min(Constants.MAX_LOAD - charge, stackSize);
                    min = Math.min(min, ((Feed) obj).quantity);
                    ((Feed) obj).quantity -= min;
                    charge += min;
                    return min != 0;
                }
            }
        }
        return false;
    }
    
    private boolean containsBlock(SparseGrid2D yard, Int2D location) {
        Bag b = yard.getObjectsAtLocation(location.x, location.y);
        if (b != null) {
            for (Object obj : b) {
                if (Block.class.isInstance(obj)) {
                    return true;
                }
            }
        }
        return false;
    }
}
