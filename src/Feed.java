import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;

public class Feed implements Steppable {
    public int x, y;
    public int quantity;
    Stoppable stoppable;


    public Feed() {
        quantity = 2;
    }

    public int takeFeed(int cap) {
        int took;
        if (this.quantity < cap) {
            took = this.quantity;
        } else {
            took = cap;
        }
        this.quantity -= took;
        return took;
    }

    private boolean isEmpty() {
        return this.quantity == 0;
    }


    private void remove(Beings beings) {
        beings.yard.remove(this);
        stoppable.stop();
        beings.createFeed();
    }


    @Override
    public void step(SimState simState) {
        Beings beings = (Beings) simState;
        if (isEmpty()) {
            remove(beings);
        }
    }
}
