import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;

public class Block implements Steppable {
    public int x, y;
    Stoppable stoppable;

    public Block() {
    }

    private void remove(Beings beings) {
        beings.yard.remove(this);
        stoppable.stop();
        beings.createFeed();
    }


    @Override
    public void step(SimState simState) {
    }
}
