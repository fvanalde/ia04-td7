public class Constants {
    public static int DISTANCE_DEPLACEMENT; //min 1
    public static int DISTANCE_PERCEPTION; //min 1
    public static int MAX_LOAD; //min 1
    public static int GRID_SIZE = 30;
    public static int NUM_INSECT = 5;
    public static int NUM_FOOD_CELL = 100;
    public static int MAX_ENERGY = 50;
    public static int CAPACITY = 10;

    public static int MAX_FOOD = 5;
    public static int FOOD_ENERGY = 3;

    public static int FRAME_SIZE = 400;

    public static int NUM_BLOCK = 100;
}
