import sim.display.Controller;
import sim.display.Display2D;
import sim.display.GUIState;
import sim.engine.SimState;
import sim.portrayal.Inspector;
import sim.portrayal.grid.SparseGridPortrayal2D;
import sim.portrayal.simple.OvalPortrayal2D;

import javax.swing.*;
import java.awt.*;

public class BeingsWithUI extends GUIState {

    public Display2D display;
    public JFrame displayFrame;
    SparseGridPortrayal2D yardPortrayal = new SparseGridPortrayal2D();

    public BeingsWithUI(SimState state) {
        super(state);
    }

    public static String getName() {
        return "Simulation de créatures";
    }

    public void start() {
        super.start();
        setupPortrayals();
    }


    public void init(Controller c) {
        super.init(c);
        display = new Display2D(Constants.FRAME_SIZE, Constants.FRAME_SIZE, this);
        display.setClipping(false);
        displayFrame = display.createFrame();
        displayFrame.setTitle("Beings");
        c.registerFrame(displayFrame);
        displayFrame.setVisible(true);
        display.attach(yardPortrayal, "Yard");
    }

    public void setupPortrayals() {
        Beings beings = (Beings) state;
        yardPortrayal.setField(beings.yard);
        yardPortrayal.setPortrayalForClass(
                Insect.class, getInsectPortrayal());
        yardPortrayal.setPortrayalForClass(
                Feed.class, getFeedPortrayal());
        yardPortrayal.setPortrayalForClass(
                Block.class, getBlockPortrayal());
        display.reset();
        display.setBackdrop(Color.LIGHT_GRAY);
        display.repaint();
    }


    public SparseGridPortrayal2D getYardPortrayal() {
        return yardPortrayal;
    }

    private OvalPortrayal2D getInsectPortrayal() {
        OvalPortrayal2D r = new OvalPortrayal2D();
        r.paint = Color.GREEN;
        r.filled = true;
        r.scale = 1;
        return r;
    }

    private OvalPortrayal2D getFeedPortrayal() {
        OvalPortrayal2D r = new OvalPortrayal2D();
        r.paint = Color.RED;
        r.filled = true;
        r.scale = 0.8;
        return r;
    }
    
    private OvalPortrayal2D getBlockPortrayal() {
        OvalPortrayal2D r = new OvalPortrayal2D();
        r.paint = Color.BLACK;
        r.filled = true;
        r.scale = 1;
        return r;
    }

    public Object getSimulationInspectedObject() {
        return state;
    }

    public Inspector getInspector() {
        Inspector i = super.getInspector();
        i.setVolatile(true);
        return i;
    }

}
