package Pathfinding;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import sim.field.grid.SparseGrid2D;
import sim.util.Bag;
import sim.util.Int2D;

public class AStar {
	
	public static ArrayList<Int2D> pathfinding(SparseGrid2D yard, Int2D depart, Int2D arrive) {
		Noeud u; // Noeud Check
		
		ArrayList<Int2D> closedList = new ArrayList<Int2D>();
		PriorityQueue<Noeud> openList = new PriorityQueue<Noeud>();
		
		openList.add(new Noeud(depart));
		
		while ( !openList.isEmpty() ) { // While there is undiscovered cell 
			u = openList.poll(); // Explore the highest priority
			
			if(u.pos.equals(arrive)) { 
				return u.getList();
			}
			
			for(Noeud v : u.getVoisin()) { // Check neighborhood
				if(isValid(yard, v.pos, arrive) && !closedList.contains(v.pos) && !existInf(openList, v.pos, u.getCout()+1) ) {
					v.parent = u; // Linked to his parent
					v.setCout(u.getCout()+1);
					v.setHeurestique(v.getCout() + distance(v.pos, arrive));
					openList.add(v); // Auto Sort
				}
			}
			// Not necessary | Just optimization
			purge(openList, u.pos);
			closedList.add(u.pos);
		}
		
		return null;
	}

	private static void purge(PriorityQueue<Noeud> openList, Int2D pos) {
		// Explore only one time each cell
		ArrayList<Noeud> toDel = new ArrayList<>();
		for(Noeud u : openList) {
			if(u.pos.equals(pos)) {
				toDel.add(u);
			}
		}
		openList.removeAll(toDel);
	}

	private static boolean isValid(SparseGrid2D yard, Int2D v, Int2D arrive) {
		// Add specific condition
		boolean valid = v.x >= 0 && v.y >= 0 && v.x < yard.getHeight() && v.y < yard.getWidth(); // Border of the yard
		valid = valid && cellIsEmpty(yard, v); // Accessible cell
		valid = valid || v.equals(arrive); // Or it the end
		return valid;
	}
	
	private static boolean cellIsEmpty(SparseGrid2D yard, Int2D v) {
		Bag b = yard.getObjectsAtLocation(v);
		// Add here to control which class are blocking and non blocking
		return (b == null || b.isEmpty() );
	}

	private static boolean existInf(PriorityQueue<Noeud> openList, Int2D vPos, int i) {
		// Check if exist another cell with inferior cout
		Noeud toDel = null;
		for(Noeud u : openList) {
			if(u.pos.equals(vPos)) {
				if (u.cout <= i) {
					return true;
				} else {
					toDel = u;
				}
			}
		}
		// On a mieux a explorer
		if (toDel != null)
			openList.remove(toDel);
		return false;
	}

	private static int distance(Int2D pos, Int2D arrive) {
		// Manhattan distance
		return Math.abs(arrive.x-pos.x) + Math.abs(arrive.y-pos.y);
	}
}
