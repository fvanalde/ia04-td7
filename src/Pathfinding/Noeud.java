package Pathfinding;

import java.util.ArrayList;

import sim.util.Int2D;

public class Noeud implements Comparable {
	Int2D pos;
	int cout;
	int heurestique;
	Noeud parent;
	
	public Noeud(Int2D pos, int cout, int heurestique) {
		super();
		this.pos = pos;
		this.cout = cout;
		this.heurestique = heurestique;
	}
	
	public Noeud(Int2D pos) {
		super();
		this.pos = pos;
	}

	public Int2D getPos() {
		return pos;
	}
	public void setPos(Int2D pos) {
		this.pos = pos;
	}
	public int getCout() {
		return cout;
	}
	public void setCout(int cout) {
		this.cout = cout;
	}
	public int getHeurestique() {
		return heurestique;
	}
	public void setHeurestique(int heurestique) {
		this.heurestique = heurestique;
	}

	@Override
	public int compareTo(Object arg0) {
		if(!Object.class.isInstance(Noeud.class)) {
			System.err.println("Comparaison Impossible");
			return 0; //throw new Exception("Comparaison impossible avec un Noeud");
		}
		Noeud n2 = (Noeud)arg0;
		if(this.getHeurestique() < n2.getHeurestique()) {
			return -1;
		} else if (this.getHeurestique() == n2.getHeurestique()) {
			return 0;
		}
		return 1;
	}

	@Override
	public String toString() {
		return "Noeud [pos=" + pos + ", heurestique=" + heurestique + "]";
	}

	public ArrayList<Noeud> getVoisin() {
		// TODO: A custom
		ArrayList<Noeud> exit = new ArrayList<Noeud>();
		exit.add(new Noeud(new Int2D(pos.x+1,pos.y)));
		exit.add(new Noeud(new Int2D(pos.x-1,pos.y)));
		exit.add(new Noeud(new Int2D(pos.x,pos.y+1)));
		exit.add(new Noeud(new Int2D(pos.x,pos.y-1)));
		return exit;
	}

	public ArrayList<Int2D> getList() {
		ArrayList<Int2D> exit = new ArrayList<Int2D>();
		Noeud u = this;
		while(u != null) {
			exit.add(u.pos);
			u = u.parent;
		}
		return exit;
	}
}
