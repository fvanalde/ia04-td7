import sim.engine.Schedule;
import sim.engine.SimState;
import sim.field.grid.SparseGrid2D;
import sim.util.Bag;
import sim.util.Int2D;

public class Beings extends SimState {

    public SparseGrid2D yard = new SparseGrid2D(Constants.GRID_SIZE, Constants.GRID_SIZE);

    private int numInsects;

    public Beings(long seed) {
        super(seed);
    }

    public void start() {
        super.start();
        yard.clear();
        addBlock();
        addInsects();
        addFood();
        numInsects = Constants.NUM_INSECT;
    }

    private void addInsects() {
        for (int i = 0; i < Constants.NUM_INSECT; i++) {
            Insect insect = new Insect();
            Int2D location = getFreeLocation(insect.getClass());
            yard.setObjectLocation(insect, location.x, location.y);
            insect.x = location.x;
            insect.y = location.y;
            insect.stoppable = schedule.scheduleRepeating(insect);
            numInsects++;
        }
        System.out.println("Depl : "+Constants.DISTANCE_DEPLACEMENT);
        System.out.println("Perc : "+Constants.DISTANCE_PERCEPTION);
        System.out.println("MaxLoad : "+Constants.MAX_LOAD);
    }

    private void addFood() {
        for (int i = 0; i < Constants.NUM_FOOD_CELL; i++) {
            createFeed();
        }
    }

    private void addBlock() {
        for (int i = 0; i < Constants.NUM_BLOCK; i++) {
        	createBlock();
        }
    }
    
    public void createFeed() {
        Feed feed = new Feed();
        Int2D location = getFreeLocation(feed.getClass());
        yard.setObjectLocation(feed, location.x, location.y);
        feed.x = location.x;
        feed.y = location.y;
        feed.stoppable = schedule.scheduleRepeating(feed);
    }
    
    public void createBlock() {
    	Block bck = new Block();
        Int2D location = getFreeLocation(bck.getClass());
        yard.setObjectLocation(bck, location.x, location.y);
        bck.x = location.x;
        bck.y = location.y;
        //bck.stoppable = Schedule.scheduleRepeating(bck);
    }

    private Int2D getFreeLocation(Class c) {
        Int2D location = new Int2D(random.nextInt(yard.getWidth()),
                random.nextInt(yard.getHeight()));
        while (containsSameType(yard, location, c) || containsSameType(yard, location, Block.class)) {
            location = new Int2D(random.nextInt(yard.getWidth()),
                    random.nextInt(yard.getHeight()));
        }
        return location;
    }

    private boolean containsSameType(SparseGrid2D yard, Int2D location, Class c) {
        Bag b = yard.getObjectsAtLocation(location.x, location.y);
        if (b != null) {
            for (Object obj : b) {
                if (c.isInstance(obj)) {
                    return true;
                }
            }
        }
        return false;
    }


    public int getNumInsects() {
        return numInsects;
    }

    public void setNumInsects(int numInsects) {
        this.numInsects = numInsects;
    }

}
